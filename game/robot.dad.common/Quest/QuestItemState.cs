namespace robot.dad.common.Quest
{
    public enum QuestItemState
    {
        NotStarted,
        Started,
        Finished
    }
}