namespace robot.dad.game.Scenes
{
    public interface ISelectableEntity
    {
        void Select();
        void Unselect();
    }
}