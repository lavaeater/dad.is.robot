namespace robot.dad.game.Scenes
{
    public enum ItemAction
    {
        Added,
        Removed,
        Selected,
        Unselected
    }
}