namespace robot.dad.game.Event
{
    public enum EventType
    {
        Ruin,
        Monster,
        Cave,
        Settlement,
        Scavenger
    }
}